package dojo.spring.mvc.hello.world.root.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="CUSTOMERS")
@NamedQueries(value = {
        @NamedQuery(name = "Customer.findBySsn",query = "select c from Customer c where c.ssn = :ssn")
})
public class Customer implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @Column(name = "SSN", nullable = false)
    private String ssn;

    @Column(name = "LAST_NAME",nullable = false)
    private String lastName;

    protected Customer() {

    }

    public Customer(String ssn, String lastName) {
        this.ssn = ssn;
        this.lastName = lastName;
    }

    public Long getId() {
        return id;
    }

    public String getSsn() {
        return ssn;
    }

    public String getLastName() {
        return lastName;
    }


    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        if (!ssn.equals(customer.ssn)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return ssn.hashCode();
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", ssn='" + ssn + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
