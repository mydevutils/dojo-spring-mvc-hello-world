package dojo.spring.mvc.hello.world.mvc.controllers;

import dojo.spring.mvc.hello.world.root.entities.Customer;
import dojo.spring.mvc.hello.world.root.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class DojoSpringMvcController {


    @Autowired(required =true)
    private CustomerService customerService;

    @RequestMapping(method = RequestMethod.POST  , value = "/hello", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public List<Customer> sampleController(@RequestBody List<Customer> customers) {

        System.out.println(">>>Called controller, customers =" + customers);

        for (Customer current : customers) {
            customerService.saveCustomer(current);
        }

        return customers;
    }

}
