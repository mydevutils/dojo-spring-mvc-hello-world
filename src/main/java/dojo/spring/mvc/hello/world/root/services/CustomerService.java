package dojo.spring.mvc.hello.world.root.services;

import dojo.spring.mvc.hello.world.root.dao.CustomerDao;
import dojo.spring.mvc.hello.world.root.entities.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service("customerService")
public class CustomerService {

    @Autowired(required = true)
    private CustomerDao customerDao;

    @Transactional
    public void saveCustomer(Customer customer) {

        System.out.println("Saving new customer ..." + customer);

        if (customerDao.findBySsn(customer.getSsn()) != null) {
            throw new IllegalArgumentException("Customer already exists with SSN:" + customer.getSsn());
        }

        customerDao.persist(customer);
    }

    public void cancel() {
        System.out.println("Cancelling new customer ...");
    }

}
