package dojo.spring.mvc.hello.world.root.dao;



import dojo.spring.mvc.hello.world.root.entities.Customer;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Component("customerDao")
public class CustomerDao {

    @PersistenceContext
    private EntityManager em;


    public Customer findBySsn(String ssn) {
        Query query = em.createNamedQuery("Customer.findBySsn");
        query.setParameter("ssn",ssn);
        List result = query.getResultList();
        if (result.size() == 0) {
            return null;
        }
        else if (result.size() == 1) {
            return (Customer) result.get(0);
        }
        throw new IllegalStateException("Only one customer per ssn can exist on the database!!");
    }

    public void persist(Customer customer) {
        em.persist(customer);
    }
}
